import { Component, Input } from '@angular/core';

import { NbMenuService, NbSidebarService, NbMenuItem } from '@nebular/theme';
import { UserService } from '../../../@core/utils/user.service';

@Component({
  selector: 'ngx-header',
  styleUrls: ['./header.component.scss'],
  templateUrl: './header.component.html',
})
export class HeaderComponent {


  @Input() position = 'normal';
  
  user: any;

  userMenu: NbMenuItem[] = [{ title: 'Resumen', link: '/admin/dashboard' }, { title: 'Cerrar Sesión', link: '/public/admin/logout' }];
  loginMenu: NbMenuItem[] = [{ title: 'Iniciar Sesión', link: '/public/admin/login' }, { title: 'Registrar', link: '/public/admin/register' }];

  constructor(private sidebarService: NbSidebarService,
              private menuService: NbMenuService,
              public userService: UserService) {
  }

  toggleSidebar(): boolean {
    this.sidebarService.toggle(true, 'menu-sidebar');
    return false;
  }

  toggleSettings(): boolean {
    this.sidebarService.toggle(false, 'settings-sidebar');
    return false;
  }

  goToHome() {
    this.menuService.navigateHome();
  }

  startSearch() {
  }
}
