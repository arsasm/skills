import { Component } from '@angular/core';
import { AccessibilityService } from '../../../@core/utils/accessibility.service';

@Component({
  selector: 'ngx-accessibility-switcher',
  styleUrls: ['./accessibility-switcher.component.scss'],
  template: `
    <label class="theme-switch">
      <div class="switch">
        <input type="checkbox" [checked]="checked" (change)="toggle(aToggle.checked)" #aToggle>
        <span class="slider"></span>
      </div>
      <span class="cosmic"></span>
    </label>
  `,
})
export class AccessibilitySwitcherComponent {
  checked: boolean;

  constructor(private accessibilityService: AccessibilityService) {
    this.updateStatus();
    setInterval(()=> {this.updateStatus()},5000)
  }
  
  updateStatus() {
    this.checked = this.accessibilityService.getConfig().isEnabled;
  }

  toggle(isToggled: boolean) {
    if (isToggled) this.accessibilityService.enableService();
    if (!isToggled) this.accessibilityService.disableService();
  }

}
