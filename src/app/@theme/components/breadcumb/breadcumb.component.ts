import { Component } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';

@Component({
  selector: 'ngx-breadcumb',
  styleUrls: ['./breadcumb.component.scss'],
  templateUrl: './breadcumb.component.html',
})
export class BreadcumbComponent {
  url: string = "Cargando...";

  parsedUrl = [];

  constructor(router: Router) {
    router.events.subscribe((e) => {
      if (e instanceof NavigationEnd) {
        this.url = e.urlAfterRedirects;
        this.refreshBreadcumb();
      }
    });
  }

  refreshBreadcumb() {
    let baseRoute = '#';
    let urlSplitted = this.url.split('/');

    this.parsedUrl = [];
    for (let i = 0; i < urlSplitted.length; i++) {
      const element = urlSplitted[i];
      
      if (element) {
        baseRoute += '/'+element;
        let newElement = {name: element.trim(), href: baseRoute, isLast: false};
        if (i == urlSplitted.length-1) newElement.isLast = true;
        this.parsedUrl.push(newElement);
      }
    }
  }

}