import { Component, HostListener } from '@angular/core';
import { KeyEventsService } from './@core/utils/keyevents.service';
import { AccessibilityService } from './@core/utils/accessibility.service';
import { Router, NavigationEnd } from '@angular/router';
import { UserService } from './@core/utils/user.service';

@Component({
  selector: 'arsa-app',
  template: '<router-outlet></router-outlet>'
})
export class AppComponent {

  // tslint:disable-next-line:max-line-length
  constructor(router: Router, private keyService: KeyEventsService, private accessibilityService: AccessibilityService, userService: UserService) {
    userService.startWorker();
    this.accessibilityService.startWorker();

    this.keyService.keyPressed$.subscribe((event) => {
      if (event.code === 'F1') { router.navigateByUrl('/accessibility'); }
    });

    router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        console.log(event);
        if (event.url === '/public/admin/logout') { userService.logout(); }
      }
    });
  }

  @HostListener('window:keydown', ['$event'])
  keyChange(event) {
    if (event.code === 'F1') { event.preventDefault(); }
    this.keyService.addEvent('keyPressed', event);
  }
}
