import { Component } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalComponent } from './modal/modal.component';

@Component({
  selector: 'ngx-admin',
  styleUrls: ['./admin.component.scss'],
  templateUrl: './admin.component.html',
})
export class AdminComponent {

  settingsVeterinarios = {
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      user: {
        title: 'Usuario',
        type: 'string',
      },
      Nombre: {
        title: 'Nombre',
        type: 'string',
      },
      Apellidos: {
        title: 'Apellidos',
        type: 'string',
      },
      Telefono: {
        title: 'Telefono',
        type: 'string',
      },
      horas: {
        title: '24 horas',
        type: 'checkbox',
      },
    },
  };

  sourceVeterinarios: LocalDataSource = new LocalDataSource();


  settingsClientes = {
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      user: {
        title: 'Chip',
        type: 'string',
      },
      Nombre: {
        title: 'Nombre',
        type: 'string',
      },
      Telefono: {
        title: 'Raza',
        type: 'string',
      },
    },
  };

  sourceClientes: LocalDataSource = new LocalDataSource();

  constructor(private modalService: NgbModal) {
    this.sourceVeterinarios.load([{ user: 'Demo', "Nombre": 'Demo', "Apellidos": 'Demo', "Telefono": 'Demo', "horas": 'Si' },]);
    this.sourceClientes.load([{ user: 'Demo', "Nombre": 'Demo', "deuda": 'Demo', "Telefono": 'Demo', "horas": '23' },]);
  }

  checkPattern(input, pattern) {
    return pattern.test(input);
  }

  comprobarDNI(dni) {
    if (this.checkPattern(dni, /(([X-Z]{1})([-]?)(\d{7})([-]?)([A-Z]{1}))|((\d{8})([A-Z]{1}))/)) {

      let numPart = dni.substring(0,8);

      let cadena = "TRWAGMYFPDXBNJZSQVHLCKET";
      let posicion = numPart % 23;
      let letra = cadena.substring(posicion, posicion + 1);
      
      console.log(numPart, letra);

      if (letra == dni.substring(8,9)) {
        this.dniCorrecto();
      } else {
        this.dniError();
      }


    } else {
      this.dniError();
    }

  }

  dniError() {
    this.showSmallModal("Dni No Válido");
  }

  dniCorrecto() {
    this.showSmallModal("Dni Válido");
  }

  onDeleteConfirm(event): void {
    if (window.confirm('Are you sure you want to delete?')) {
      event.confirm.resolve();
    } else {
      event.confirm.reject();
    }
  }

  showSmallModal(contenido) {
    const activeModal = this.modalService.open(ModalComponent, { size: 'sm', container: 'nb-layout' });

    activeModal.componentInstance.modalHeader = 'Comprobación DNI';
    activeModal.componentInstance.modalContent = contenido;
  }
}
