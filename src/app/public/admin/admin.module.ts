import { NgModule } from '@angular/core';

import { NgxEchartsModule } from 'ngx-echarts';

import { ThemeModule } from '../../@theme/theme.module';
import { AdminComponent } from './admin.component';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { ModalComponent } from './modal/modal.component';


@NgModule({
  imports: [
    ThemeModule,
    NgxEchartsModule,
    Ng2SmartTableModule,
  ],
  declarations: [
    ModalComponent,
    AdminComponent
  ],
  entryComponents: [
    ModalComponent,
  ]
})
export class AdminModule { }
