import { Component } from '@angular/core';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { VeterinariosVHComponent } from '../veterinariosVH/veterinariosVH.component';
import { ApiqueryService } from '../../@core/utils/apiquery.service';

@Component({
  selector: 'ngx-veterinarios',
  styleUrls: ['./veterinariosC.component.scss'],
  templateUrl: './veterinariosC.component.html',
})
export class VeterinariosCComponent {

  perro: any;
  cliente: any;

  consulta: any = {Id_Perro: 0, Observaciones: '', Importe: '', Pagada: true, Baja: false};

  constructor(private activeModal: NgbActiveModal, private modalService: NgbModal, private apiQueryService: ApiqueryService) {
  }


  saveConsulta() {
    this.apiQueryService.addConsulta(this.consulta).subscribe(() => {
      this.activeModal.close();
    }, () => {});
  }


  verHistorial() {
    const modal = this.modalService.open(VeterinariosVHComponent, { size: 'lg', container: 'nb-layout' });
    modal.componentInstance.perro = this.perro;

    this.apiQueryService.getAllByDog(this.perro.Id).subscribe((res) => {
      modal.componentInstance.consultas = res['Avisos/GetHistory'];
    }, () => {});

  }
}
