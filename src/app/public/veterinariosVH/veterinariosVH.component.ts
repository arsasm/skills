import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'ngx-veterinarios',
  styleUrls: ['./veterinariosVH.component.scss'],
  templateUrl: './veterinariosVH.component.html',
})
export class VeterinariosVHComponent {

  perro: any;
  consultas: any = [];

  constructor(private activeModal: NgbActiveModal) {
  }

  closeModal() {
    this.activeModal.close();
  }

}
