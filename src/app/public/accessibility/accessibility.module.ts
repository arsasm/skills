import { NgModule } from '@angular/core';

import { NgxEchartsModule } from 'ngx-echarts';

import { ThemeModule } from '../../@theme/theme.module';
import { AccessibilityComponent } from './accessibility.component';
import { StatusCardComponent } from './status-card/status-card.component';



@NgModule({
  imports: [
    ThemeModule,
    NgxEchartsModule,
  ],
  declarations: [
    AccessibilityComponent,
    StatusCardComponent
  ],
})
export class AccessibilityModule { }
