import { Component } from '@angular/core';
import { AccessibilityService } from '../../@core/utils/accessibility.service';

@Component({
  selector: 'ngx-accessibility',
  styleUrls: ['./accessibility.component.scss'],
  templateUrl: './accessibility.component.html',
})
export class AccessibilityComponent {
  audioServiceStatus: boolean;
  autoplayAudioServiceStatus: boolean;
  autoplayTextServiceStatus: boolean = false;
  autoplayDaltServiceStatus: boolean = false;
  
  constructor(public accessibilityService: AccessibilityService) {
    this.accessibilityService.disableService();
    this.audioServiceStatus = this.getAudioServiceStatus();
    this.autoplayAudioServiceStatus = this.getAutoAudioServiceStatus();
    this.autoplayAudioServiceStatus = this.getAutoAudioServiceStatus();
    // this.autoplayTextServiceStatus = this.getTextServiceStatus();
    // this.autoplayDaltServiceStatus = this.getTextServiceStatus();
  }


  toggleAudioService(status: boolean) {
    if (status) this.accessibilityService.enableAudioService();
    if (!status) this.accessibilityService.disableAudioService();
  }
  
  getAudioServiceStatus(): boolean {
    return this.accessibilityService.getConfig().audio.isEnabled;
  }
  
  getTextServiceStatus(): boolean {
    return this.accessibilityService.getConfig().text.isEnabled;
  }

  toggleAutoAudioService(status: boolean) {
    if (status) this.accessibilityService.enableAutoplayAudioService();
    if (!status) this.accessibilityService.disableAutoplayAudioService();
  }

  toggleTextService(status: boolean) {
    if (status) this.accessibilityService.enableTextService();
    if (!status) this.accessibilityService.disableTextService();
  }

  toggleDaltService(status: boolean) {
    if (status) this.accessibilityService.enableDaltService();
    if (!status) this.accessibilityService.disableDaltService();
  }
  
  getAutoAudioServiceStatus(): boolean {
    return this.accessibilityService.getConfig().audio.autoplay;
  }

  changeRange(val) {
    this.accessibilityService.changeTextServiceMultiplier(val.target.value);
  }
  
}
