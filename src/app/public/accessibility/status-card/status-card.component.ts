import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'ngx-status-card',
  styleUrls: ['./status-card.component.scss'],
  template: `
    <nb-card (click)="onChangeStatus()" [ngClass]="{'off': !checked}">
      <div class="icon-container">
        <div class="icon {{ type }}">
          <ng-content></ng-content>
        </div>
      </div>

      <div class="details">
        <div class="title aService">{{ title }}</div>
        <div class="status aService">{{ checked ? 'ACTIVADO' : 'DESACTIVADO' }}</div>
      </div>
    </nb-card>
  `,
})
export class StatusCardComponent {

  checked: boolean = false;
  @Input() title: string;
  status: string;
  @Input() type: string;
  @Input() on: boolean = true;

  @Output() change: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor() {
    // this.checked = this.on;
    // console.log("Checked", this.on)
  }

  onChangeStatus() {
    this.checked = !this.checked;
    this.change.emit(this.checked);
  }

}
