import { Component } from '@angular/core';
import { UserService, UserInterface } from '../../@core/utils/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'ngx-login-page',
  styleUrls: ['./login.component.scss'],
  templateUrl: './login.component.html',
})
export class LoginComponent {

  config = {
    password: {
      minLength: 6,
      maxLength: 16
    }
  };

  user: UserInterface = {
    loginId: '',
    password: '',
    role: 'Usuario Demo'
  };

  constructor(private userService: UserService, private router: Router) {

  }

  login() {
    if (this.userService.login(this.user)) {
      // TODO Success Toast
      // TODO: Redirect to Role Page
      this.router.navigateByUrl('/admin/dashboard');
    } else {
      // TODO Error Toast
    }
  }

}
