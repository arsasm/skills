import { NgModule } from '@angular/core';

import { PublicComponent } from './public.component';
import { DashboardModule } from './dashboard/dashboard.module';
import { PublicRoutingModule } from './public-routing.module';
import { ThemeModule } from '../@theme/theme.module';
import { AccessibilityModule } from './accessibility/accessibility.module';
import { LoginModule } from './login/login.module';
import { AdminModule } from './admin/admin.module';
import { VeterinariosModule } from './veterinarios/veterinarios.module';
import { VeterinariosCModule } from './veterinariosC/veterinariosC.module';
import { VeterinariosVHModule } from './veterinariosVH/veterinariosVH.module';
import { VeterinariosNPModule } from './veterinariosNP/veterinariosNP.module';
import { AdminGPerrosModule } from './adminGPerros/adminGPerros.module';
import { ClientesModule } from './clientes/clientes.module';

const PAGES_COMPONENTS = [
  PublicComponent,
];

@NgModule({
  imports: [
    PublicRoutingModule,
    ThemeModule,
    DashboardModule,
    AccessibilityModule,
    LoginModule,
    AdminModule,
    VeterinariosModule,
    VeterinariosCModule,
    VeterinariosVHModule,
    AdminGPerrosModule,
    ClientesModule,
    VeterinariosNPModule,
  ],
  declarations: [
    ...PAGES_COMPONENTS,
  ],
})
export class PublicModule {
}
