import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { PublicComponent } from './public.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AccessibilityComponent } from './accessibility/accessibility.component';
import { AdminComponent } from './admin/admin.component';
import { VeterinariosComponent } from './veterinarios/veterinarios.component';
import { VeterinariosCComponent } from './veterinariosC/veterinariosC.component';
import { VeterinariosVHComponent } from './veterinariosVH/veterinariosVH.component';
import { AdminGPerrosComponent } from './adminGPerros/adminGPerros.component';
import { ClientesComponent } from './clientes/clientes.component';
import { VeterinariosNPComponent } from './veterinariosNP/veterinariosNP.component';

const routes: Routes = [{
  path: '',
  component: PublicComponent,
  children: [{
    path: 'dashboard',
    component: DashboardComponent,
  },
  {
    path: 'accessibility',
    component: AccessibilityComponent,
  }, 
  {
    path: 'cliente',
    component: ClientesComponent,
  },  
  {
    path: 'admin',
    component: AdminComponent,
  },  
  {
    path: 'admin/gperros',
    component: AdminGPerrosComponent,
  }, 
  {
    path: 'veterinarios',
    component: VeterinariosComponent,
  }, 
  {
    path: 'veterinarios/consulta',
    component: VeterinariosCComponent,
  }, 
  {
    path: 'veterinarios/nperros',
    component: VeterinariosNPComponent,
  }, 
  {
    path: 'veterinarios/historial',
    component: VeterinariosVHComponent,
  }, 
  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full',
  },
  {
    path: 'admin',
    redirectTo: 'admin/login',
    pathMatch: 'full'
  }],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PublicRoutingModule {
}
