import { Component } from '@angular/core';
import { trigger, transition, query, style, animate } from '@angular/animations';
import { ApiqueryService } from '../../@core/utils/apiquery.service';
import { UserService } from '../../@core/utils/user.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalComponent } from '../admin/modal/modal.component';

@Component({
  selector: 'ngx-admin',
  animations: [
    trigger(
      'ambulance', [
        transition(
          '* => first', [
            query(':self', [style({
              bottom: 0,
              // rigth: 0
            }), animate('3s', style({
              bottom: '50%',
              right: 0
            }))], {
                limit: 0
              }),
          ]),
        transition(
          '* => toUp', [
            query(':self', [style({
              bottom: '50%',
              right: 0
            }), animate('3s', style({
              top: 0,
              right: '50%'
            }))], {
                limit: 0
              }),
          ]),
        transition(
          '* => toLeft', [
            query(':self', [style({
              top: 0,
              right: '50%'
            }), animate('3s', style({
              top: '50%',
              left: 0
            }))], {
                limit: 0
              }),
          ]),
        transition(
          '* => toDown', [
            query(':self', [style({
              top: '50%',
              left: 0
            }), animate('3s', style({
              top: '93%',
              left: '50%'
            }))], {
                limit: 0
              }),
          ]),
        transition(
          '* => toRigth', [
            query(':self', [style({
              top: '93%',
              left: '50%'
            }), animate('3s', style({
              top: '50%',
              left: '90%'
            }))], {
                limit: 0
              }),
          ]),
      ]),
  ],
  styleUrls: ['./clientes.component.scss'],
  templateUrl: './clientes.component.html',
})
export class ClientesComponent {

  animate = 'first';
  showTransition = false;

  direccion = '';

  config = {Contrasena: '', Contrasena1: '', Telefono: ''};

  constructor(private modalService: NgbModal, private userService: UserService, private apiQueryService: ApiqueryService) {
    this.config = this.userService.getSession().user.remote;
    // TODO: Get config from session
    this.config.Contrasena = '';
    this.config.Contrasena1 = '';
  }

  crearAviso() {
    this.apiQueryService.putAviso({Direccion: this.direccion}).subscribe((res) => {
      if (res['Avisos/Add']) {
        this.showTransition = true;
      } else {
        const activeModal = this.modalService.open(ModalComponent, { size: 'sm', container: 'nb-layout' });
  
        activeModal.componentInstance.modalHeader = 'Error';
        activeModal.componentInstance.modalContent = 'Tiene alguna consulta sin pagar, diríjase a uno de nuestros centros para saldar la deuda';
      }
    }, () => { });
  }

  updateConfig() {
    if (this.config.Contrasena === this.config.Contrasena1) {
      this.apiQueryService.modifyUser(this.config).subscribe((res) => {
        this.userService.setNewRemote(res['User/Update']);
        if (this.config.Contrasena!='') this.userService.setNewPassword(this.config.Contrasena);
        // Success toast
      }, () => {
        // Error toast
      });
    } else {
      const activeModal = this.modalService.open(ModalComponent, { size: 'sm', container: 'nb-layout' });

      activeModal.componentInstance.modalHeader = 'Error';
      activeModal.componentInstance.modalContent = 'Las contrasenas no coinciden';
    }
  }

  animationDone(event) {
    if (event.toState === 'first') { this.animate = 'toUp'; }
    if (event.toState === 'toUp') { this.animate = 'toLeft'; }
    if (event.toState === 'toLeft') { this.animate = 'toDown'; }
    if (event.toState === 'toDown') { this.animate = 'toRigth'; }
    if (event.toState === 'toRigth') { this.animate = 'toUp'; }
  }

}
