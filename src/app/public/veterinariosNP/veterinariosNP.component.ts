import { Component } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';

@Component({
  selector: 'ngx-veterinarios',
  styleUrls: ['./veterinariosNP.component.scss'],
  templateUrl: './veterinariosNP.component.html',
})
export class VeterinariosNPComponent {
  user: "";

  visitaDespues: boolean = false;

  settingsVeterinarios = {
    add: {
      addButtonContent: '',
      createButtonContent: '',
      cancelButtonContent: '',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      user: {
        title: 'Usuario',
        type: 'string',
      },
      Nombre: {
        title: 'Nombre',
        type: 'string',
      },
      Apellidos: {
        title: 'Apellidos',
        type: 'string',
      },
      Telefono: {
        title: 'Telefono',
        type: 'string',
      },
      horas: {
        title: '24 horas',
        type: 'checkbox',
      },
    },
  };

  sourceVeterinarios: LocalDataSource = new LocalDataSource();



  constructor() {
    this.sourceVeterinarios.load([{user:'Demo', "Nombre": 'Demo', "Apellidos": 'Demo', "Telefono":'Demo', "horas":'Si'}, ]);
    // this.sourceClientes.load([{user:'Demo', "Nombre": 'Demo', "deuda": 'Demo', "Telefono":'Demo', "horas":'23'}, ]);
  }

  onDeleteConfirm(event): void {
    if (window.confirm('Are you sure you want to delete?')) {
      event.confirm.resolve();
    } else {
      event.confirm.reject();
    }
  }
}
