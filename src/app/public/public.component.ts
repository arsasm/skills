import { Component } from '@angular/core';

import { MENU_ITEMS } from './public-menu';
import { NbThemeService, NbMenuItem } from '@nebular/theme';
import { UserService } from '../@core/utils/user.service';

@Component({
  selector: 'ngx-admin',
  template: `
    <default-layout>
      <nb-menu [items]="menu"></nb-menu>
      <router-outlet></router-outlet>
    </default-layout>
  `,
})
export class PublicComponent {
  menu: NbMenuItem[] = MENU_ITEMS;
  
  constructor(private themeService: NbThemeService, private userService: UserService) {
    this.themeService.changeTheme('default');
    // this.evalMenuElement(this.menu);

    // this.userService.$sessionChange.subscribe(() => {this.evalMenuElement(this.menu)});
  }
  
  // evalMenuElement(elements: NbMenuItem[]) {
  //   for (let i = 0; i < elements.length; i++) {
  //     if (elements[i].data) {
  //       elements[i].hidden = false;
  //       if (elements[i].data.requireLogout && this.isLoggedIn()) elements[i].hidden = true;
  //       if (elements[i].data.requireLogin && !this.isLoggedIn()) elements[i].hidden = true;
  //     }

  //     if (elements[i].children) this.evalMenuElement(elements[i].children);
  //   }
  // }


  // ngOnDestroy(): void {
  //   this.menu.forEach(element => {
  //     element.selected = false;
  //     element.expanded = false;
  //   });
  // }

  isLoggedIn(): boolean {
    return this.userService.getSession().isLoggedIn;
  }
  
}
