import { Component } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalComponent } from '../admin/modal/modal.component';
import { ApiqueryService } from '../../@core/utils/apiquery.service';
import { UserService } from '../../@core/utils/user.service';
import { VeterinariosCComponent } from '../veterinariosC/veterinariosC.component';

@Component({
  selector: 'ngx-veterinarios',
  styleUrls: ['./veterinarios.component.scss'],
  templateUrl: './veterinarios.component.html',
})
export class VeterinariosComponent {

  config = { Nombre: '', Apellidos: '', Contrasena: '', Contrasena1: '', Telefono: '', Disponible: false };

  visitaDespues = false;
  visitaDespuesData: any;

  constructor(private modalService: NgbModal, private userService: UserService, private apiQueryService: ApiqueryService) {
    this.config = this.userService.getSession().user.remote;
    this.config.Contrasena = '';
    this.config.Contrasena1 = '';
  }

  onDeleteConfirm(event): void {
    if (window.confirm('Are you sure you want to delete?')) {
      event.confirm.resolve();
    } else {
      event.confirm.reject();
    }
  }

  nextAviso() {
    this.apiQueryService.getFirstAviso().subscribe((res) => {
      if (res.Success) {
        this.visitaDespues = true;
        this.visitaDespuesData = res['Avisos/GetFirst'];
      } else {
        this.showSmallModal();
      }
    }, () => {});
  }

  updateConfig() {
    if (this.config.Contrasena === this.config.Contrasena1) {
      this.apiQueryService.modifyUser(this.config).subscribe((res) => {
        this.userService.setNewRemote(res['User/Update']);
        if (this.config.Contrasena != '') this.userService.setNewPassword(this.config.Contrasena);
        // Success toast
      }, () => {
        // Error toast
      });
    } else {
      const activeModal = this.modalService.open(ModalComponent, { size: 'sm', container: 'nb-layout' });

      activeModal.componentInstance.modalHeader = 'Error';
      activeModal.componentInstance.modalContent = 'Las contrasenas no coinciden';
    }
  }

  openModalCreateConsulta(perro) {
    const activeModal = this.modalService.open(VeterinariosCComponent, { size: 'lg', container: 'nb-layout' });

    activeModal.componentInstance.perro = perro;
    activeModal.componentInstance.cliente = this.visitaDespuesData;
    activeModal.componentInstance.consulta.Id_Perro = perro.Id;
  }

  showSmallModal() {
    const activeModal = this.modalService.open(ModalComponent, { size: 'sm', container: 'nb-layout' });

    activeModal.componentInstance.modalHeader = 'Error';
    activeModal.componentInstance.modalContent = 'No hay visitas pendientes';
  }
}
