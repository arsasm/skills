import { NbMenuItem } from '@nebular/theme';

export const MENU_ITEMS: NbMenuItem[] = [
  {
    title: 'Inicio',
    icon: 'fa fa-home',
    link: '/public/dashboard',
    home: true,
  },
  {
    title: 'NAVEGACIÓN',
    group: true,
  },
  {
    title: 'Contenido',
    icon: 'fa fa-bars',
    link: '/demopages/ui-features',
    children: [
      {
        title: 'Buttons',
        link: '/demopages/ui-features/buttons',
      },
      {
        title: 'Grid',
        link: '/demopages/ui-features/grid',
      },
      {
        title: 'Icons',
        link: '/demopages/ui-features/icons',
      },
      {
        title: 'Modals',
        link: '/demopages/ui-features/modals',
      },
      {
        title: 'Popovers',
        link: '/demopages/ui-features/popovers',
      },
      {
        title: 'Typography',
        link: '/demopages/ui-features/typography',
      },
      {
        title: 'Animated Searches',
        link: '/demopages/ui-features/search-fields',
      },
      {
        title: 'Tabs',
        link: '/demopages/ui-features/tabs',
      },
    ],
  },
  {
    title: 'Admin Area',
    icon: 'fa fa-lock',
    children: [
      {
        title: 'Resumen',
        link: '/admin/dashboard',
        data: {
          requireLogin: true,
          requireLogout: false
        }
      },
      {
        title: 'Iniciar Sesión',
        link: '/public/admin/login',
        data: {
          requireLogin: false,
          requireLogout: true
        }
      },
      {
        title: 'Registrar',
        link: '/auth/register',
        data: {
          requireLogin: false,
          requireLogout: true
        }
      },
      {
        title: 'Recuperar Contraseña',
        link: '/auth/request-password',
        data: {
          requireLogin: false,
          requireLogout: true
        }
      },
      {
        title: 'Cerrar Sesión',
        link: '/public/admin/logout',
        data: {
          requireLogin: true,
          requireLogout: false
        }
      },
    ],
  },
  {
    title: 'Accesibilidad',
    icon: 'fa fa-wheelchair-alt',
    link: '/public/accessibility',
  },
];
