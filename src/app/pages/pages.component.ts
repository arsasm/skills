import { Component, OnDestroy } from '@angular/core';

import { MENU_ITEMS } from './pages-menu';
import { KeyEventsService } from '../@core/utils/keyevents.service';
import { NbThemeService } from '@nebular/theme';

@Component({
  selector: 'ngx-pages',
  template: `
    <default-layout>
      <nb-menu [items]="menu"></nb-menu>
      <router-outlet></router-outlet>
    </default-layout>
  `,
})
export class PagesComponent implements OnDestroy {

  menu = MENU_ITEMS;
  keyEventSubscription = null;
  
  constructor(private themeService: NbThemeService, appcp: KeyEventsService) {
    this.themeService.changeTheme('cosmic');
    this.keyEventSubscription = appcp.keyPressed$.subscribe((keyEvent) => {console.log("RECEIVED: ", keyEvent)}, ()=>{});
  }

  ngOnDestroy(): void {
    if (this.keyEventSubscription!=null) this.keyEventSubscription.unsubscribe();
  
    this.menu.forEach(element => {
      element.expanded = false;
    });
  }


}
