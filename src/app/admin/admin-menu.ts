import { NbMenuItem } from '@nebular/theme';

export const MENU_ITEMS: NbMenuItem[] = [
  {
    title: 'Volver',
    icon: 'fa fa-arrow-left',
    link: '/public/dashboard',
    home: true,
  },
  {
    title: 'Inicio',
    icon: 'fa fa-home',
    link: '/admin/dashboard',
    home: true,
  },
  {
    title: 'FEATURES',
    group: true,
  },
  {
    title: 'UI Features',
    icon: 'nb-keypad',
    link: '/demopages/ui-features',
    children: [
      {
        title: 'Buttons',
        link: '/demopages/ui-features/buttons',
      },
      {
        title: 'Grid',
        link: '/demopages/ui-features/grid',
      },
      {
        title: 'Icons',
        link: '/demopages/ui-features/icons',
      },
      {
        title: 'Modals',
        link: '/demopages/ui-features/modals',
      },
      {
        title: 'Popovers',
        link: '/demopages/ui-features/popovers',
      },
      {
        title: 'Typography',
        link: '/demopages/ui-features/typography',
      },
      {
        title: 'Animated Searches',
        link: '/demopages/ui-features/search-fields',
      },
      {
        title: 'Tabs',
        link: '/demopages/ui-features/tabs',
      },
    ],
  },
  {
    title: 'Forms',
    icon: 'nb-compose',
    children: [
      {
        title: 'Form Inputs',
        link: '/demopages/forms/inputs',
      },
      {
        title: 'Form Layouts',
        link: '/demopages/forms/layouts',
      },
    ],
  },
  {
    title: 'Components',
    icon: 'nb-gear',
    children: [
      {
        title: 'Tree',
        link: '/demopages/components/tree',
      }, {
        title: 'Notifications',
        link: '/demopages/components/notifications',
      },
    ],
  },
  {
    title: 'Maps',
    icon: 'nb-location',
    children: [
      {
        title: 'Google Maps',
        link: '/demopages/maps/gmaps',
      },
      {
        title: 'Leaflet Maps',
        link: '/demopages/maps/leaflet',
      },
      {
        title: 'Bubble Maps',
        link: '/demopages/maps/bubble',
      },
      {
        title: 'Search Maps',
        link: '/demopages/maps/searchmap',
      },
    ],
  },
  {
    title: 'Charts',
    icon: 'nb-bar-chart',
    children: [
      {
        title: 'Echarts',
        link: '/demopages/charts/echarts',
      },
      {
        title: 'Charts.js',
        link: '/demopages/charts/chartjs',
      },
      {
        title: 'D3',
        link: '/demopages/charts/d3',
      },
    ],
  },
  {
    title: 'Editors',
    icon: 'nb-title',
    children: [
      {
        title: 'TinyMCE',
        link: '/demopages/editors/tinymce',
      },
      {
        title: 'CKEditor',
        link: '/demopages/editors/ckeditor',
      },
    ],
  },
  {
    title: 'Tables',
    icon: 'nb-tables',
    children: [
      {
        title: 'Smart Table',
        link: '/demopages/tables/smart-table',
      },
    ],
  },
  {
    title: 'Auth',
    icon: 'nb-locked',
    children: [
      {
        title: 'Login',
        link: '/auth/login',
      },
      {
        title: 'Register',
        link: '/auth/register',
      },
      {
        title: 'Request Password',
        link: '/auth/request-password',
      },
      {
        title: 'Reset Password',
        link: '/auth/reset-password',
      },
    ],
  },
  {
    title: 'Accesibilidad',
    icon: 'fa fa-wheelchair-alt',
    link: '/public/accessibility',
    home: true,
  },
];
