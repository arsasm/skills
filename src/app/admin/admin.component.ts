import { Component, OnDestroy } from '@angular/core';

import { MENU_ITEMS } from './admin-menu';
import { NbThemeService } from '@nebular/theme';

@Component({
  selector: 'ngx-admin',
  template: `
    <default-layout>
      <nb-menu [items]="menu"></nb-menu>
      <router-outlet></router-outlet>
    </default-layout>
  `,
})
export class AdminComponent implements OnDestroy {

  menu = MENU_ITEMS;

  constructor(private themeService: NbThemeService) {
    this.themeService.changeTheme('cosmic');
  }

  ngOnDestroy(): void {
    this.menu.forEach(element => {
      element.expanded = false;
    });
  }
  
}
