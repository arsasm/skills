import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { GLOBAL } from './global';
import { Observable } from 'rxjs/Observable';
import { SessionInterface } from './user.service';

@Injectable()
export class ApiqueryService {

  constructor(private _http: Http) { }

  /* USERS */
  modifyUser(user) {
    return this.doQuery('User/Update', true, user);
  }
  getUser() {
    return this.doQuery('user/get', true);
  }

  /* AVISOS */
  putAviso(body) {
    return this.doQuery('avisos/Add', true, body);
  }
  getFirstAviso() {
    return this.doQuery('Avisos/GetFirst', true);
  }
  getAllByDog(id) {
    return this.doQuery('Avisos/GetHistory', true, {Id:id});
  }
  addConsulta(body) {
    return this.doQuery('Avisos/AddConsulta', true, body);
  }


  private doQuery(action: String, autho: boolean, body: any = null): Observable<any> {
    console.log('Req-APIRest[' + action + '@' + autho + ']: Body', body);

    const headers = new Headers({
      'Content-Type': 'application/json'
    });

    let session = this.getSession();

    if (autho && session.user) { 
      headers.append('loginid', session.user.loginId);
      headers.append('password', session.user.password);
    }

    return this.doQueryResponse(action, autho, headers, body);
  }


  getSession(): SessionInterface {
    return JSON.parse(localStorage.getItem('userService'));
  }

  private doQueryResponse(action, autho, headers, body = null): Observable<any> {
    const query = (body == null)
      ? this._http.get(GLOBAL.url + '?a=' + action, { headers: headers })
        .map(res => res.json())
      : this._http.post(GLOBAL.url + '?a=' + action, JSON.stringify(body), { headers: headers })
        .map(res => res.json());

    return Observable.create(observer => {
      query.subscribe(
        Response => {
          if (!Response.Success) {
            console.log('Res[E]-APIRest[' + action + '@' + autho + ']: Response', Response);
            // if (notifyError) this.events.publish('Error:API', Response.Error);
            observer.error(Response);
          } else {
            console.log('Res[S]-APIRest[' + action + '@' + autho + ']: Response', Response);
            observer.next(Response);
          }


        }, Error => {
          const error = {
            'Success': false,
            'Warning': false,
            'Error': {
              'Number': -1,
              'Message': 'NETWORK ERROR',
              'Error': Error
            }
          };
          console.log('Res[NE]-APIRest[' + action + ((autho) ? '@' + autho : '') + ']: Response', error);
        //   if (notifyError) this.events.publish('Error:API', error.Error);
          observer.error(error);
        }
      );
    });
  }

}
