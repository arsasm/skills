import { Injectable, EventEmitter } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';


export interface AccessibilityAudioSettings {
    isEnabled: boolean;
    autoplay: boolean;
}

export interface AccessibilityTextSettings {
    isEnabled: boolean;
    multiplier: number;
}

export interface AccessibilitySettings {
    isEnabled: boolean;
    audio: AccessibilityAudioSettings;
    text: AccessibilityTextSettings;
    dalt: AccessibilityTextSettings;
}

@Injectable()
export class AccessibilityService {
    public static readonly AUDIO_API_HOST = 'https://talkify.net';
    public static readonly AUDIO_API_PASS = '8924f5c8-ce0d-4ac0-bc87-ab78663d4e25';

    private workerStarted: boolean = false;
    private audioServiceLocked: boolean = false;
    private textServiceLocked: boolean = false;


    public $configChange: EventEmitter<AccessibilitySettings> = new EventEmitter();

    public config: AccessibilitySettings = {
        isEnabled: true,
        audio: {
            isEnabled: false,
            autoplay: true,
        },
        text: {
            isEnabled: true,
            multiplier: 1.2,
        },
        dalt: {
            isEnabled: true,
            multiplier: 1.2,
        }
    };

    private _audioInterval: any = null;
    private _audioPlayer: any = null;
    private _audioPlaylist: any = null;
    private _audioStatus: "started" | "stopped" = 'stopped';

    constructor(router: Router) {
        this.$configChange.subscribe(() => { this.onConfigChange() }, () => { });
        router.events.subscribe((e) => { this.onRouteChanged(e), () => { } });
    }

    startWorker() {
        if (this.workerStarted) return;
        let toConfig = JSON.parse(localStorage.getItem('accessibilityService'));
        if (toConfig) this.config = toConfig;

        this.$configChange.emit(this.config);
        this.workerStarted = true;
    }

    onRouteChanged(e) {
        if (e instanceof NavigationEnd && this._audioStatus == "started") {
            this._audioPlayer.pause();
            setTimeout(() => { this._audioPlaylist.play() }, 3000);
            // setTimeout(() => { if (this.config.text.isEnabled) this.startTextService(); else this.stopTextService(); }, 3000);
        }
    }

    enableService() {
        if (this.isLocked()) return;
        this.config.isEnabled = true;
        this.$configChange.emit(this.config);
    }

    disableService() {
        if (this.isLocked()) return;
        this.config.isEnabled = true;
        this.$configChange.emit(this.config);
    }

    enableAudioService() {
        if (this.isLocked()) return;
        this.config.audio.isEnabled = true;
        this.$configChange.emit(this.config);
    }

    enableTextService() {
        if (this.isLocked()) return;
        this.config.text.isEnabled = true;
        this.$configChange.emit(this.config);
    }

    changeTextServiceMultiplier(val) {
        if (this.isLocked()) return;
        this.config.text.multiplier = val;
        this.$configChange.emit(this.config);
    }
    
    disableTextService() {
        if (this.isLocked()) return;
        console.log("Disable text")
        this.config.text.isEnabled = false;
        this.$configChange.emit(this.config);
    }

    enableDaltService() {
        if (this.isLocked()) return;
        this.config.dalt.isEnabled = true;
        this.$configChange.emit(this.config);
    }
    
    disableDaltService() {
        if (this.isLocked()) return;
        this.config.dalt.isEnabled = false;
        this.$configChange.emit(this.config);
    }

    disableAudioService() {
        if (this.isLocked()) return;
        this.config.audio.isEnabled = false;
        this.$configChange.emit(this.config);
    }

    enableAutoplayAudioService() {
        if (this.isLocked()) return;
        this.config.audio.autoplay = true;
        this.$configChange.emit(this.config);
    }

    disableAutoplayAudioService() {
        if (this.isLocked()) return;
        this.config.audio.autoplay = false;
        this.$configChange.emit(this.config);
    }

    getConfig(): AccessibilitySettings {
        return this.config;
    }

    private onConfigChange() {
        if (this.isLocked()) return;

        console.log("configChange", this.config);

        localStorage.setItem('accessibilityService', JSON.stringify(this.config));

        if (this.config.isEnabled) {
            if (this.config.audio.isEnabled && this._audioStatus != "started") this.startAudioService();
            if (this.config.text.isEnabled) this.startTextService();
            if (this.config.dalt.isEnabled) this.startDaltService();

            if (!this.config.audio.isEnabled && this._audioStatus != "stopped") this.stopAudioService();
            if (!this.config.text.isEnabled) this.stopTextService();
            if (!this.config.dalt.isEnabled) this.stopDaltService();
        } else {
            if (this._audioStatus != "stopped") this.stopAudioService();
            this.stopTextService();
            this.stopDaltService();
        }

    }

    isLocked(): boolean {
        return this.audioServiceLocked || this.textServiceLocked;
    }


    //**** AUDIO SERVICE FUNCTIONS ****//
    private startAudioService() {
        this.audioServiceLocked = true;
        talkify.config.remoteService.host = AccessibilityService.AUDIO_API_HOST;
        talkify.config.remoteService.apiKey = AccessibilityService.AUDIO_API_PASS;

        talkify.config.ui.audioControls = {
            enabled: false, //<-- Disable to get the browser built in audio controls
            //container: document.getElementById("player")
        };

        if (this._audioPlayer == null) {
            this._audioPlayer = new talkify.Html5Player().enableTextHighlighting();
        } else {
            this._audioPlayer.enableTextHighlighting();
            this._audioPlayer.setVolume(1);
        }

        this.reloadContentToPlaylist(true);
        this._audioInterval = setInterval(() => {
            this.reloadContentToPlaylist(false);
        }, 2000);

        this._audioStatus = 'started';
    }

    private reloadContentToPlaylist(firstTime) {
        if (!firstTime) {
            this._audioPlaylist.disableTextInteraction();
            this._audioPlaylist.dispose();
        }
        this._audioPlaylist = new talkify.playlist()
            .begin()
            .usingPlayer(this._audioPlayer)
            .withRootSelector('.page-content')
            .withElements(document.querySelectorAll('.aService'))
            .withTextInteraction()
            .build();

        if (firstTime && this.config.audio.autoplay) this._audioPlaylist.play();
        this.audioServiceLocked = false;
    }

    private stopAudioService() {
        this.audioServiceLocked = true;
        clearInterval(this._audioInterval);
        this._audioPlaylist.disableTextInteraction();
        this._audioPlaylist.pause();
        this._audioPlaylist.dispose();
        this._audioPlayer.pause();
        this._audioPlayer.disableTextHighlighting();
        this._audioPlayer.setVolume(0);

        this._audioStatus = 'stopped';
        this.audioServiceLocked = false;
    }

    //**** TEXT SERVICE FUNCTIONS ****//
    private startTextService() {
        this.setCssVariables(this.config.text.multiplier);
    }

    private stopTextService() {
        this.setCssVariables(1);
    }

    private setCssVariables(multiplier: number) {
        let container: any = document.querySelector('body');

        if (container) this.getDefaultTextSize().forEach((element) => {
            container.style.setProperty('--dinamic-font-'+element.name, element.value * multiplier + element.type);
        });


    }

    private getDefaultTextSize() {
        return [
            { type: 'rem', name: 'h1', value: 2.5 },
            { type: 'rem', name: 'h2', value: 2 },
            { type: 'rem', name: 'h3', value: 1.75 },
            { type: 'rem', name: 'h4', value: 1.5 },
            { type: 'rem', name: 'h5', value: 1.25 },
            { type: 'rem', name: 'h6', value: 1 },
            { type: 'rem', name: 'p', value: 1 },
        ]
    }
    private startDaltService() {
        this.setCssDaltVariable(true);
    }

    private stopDaltService() {
        this.setCssDaltVariable(false);
    }

    private setCssDaltVariable(on: boolean) {
        let container: any = document.querySelector('body');


        if (container) {
            container.style.setProperty('--gray-percent', (on) ? '100%' : '0%');
        }

    }


}
