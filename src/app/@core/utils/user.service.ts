import { Injectable, EventEmitter } from '@angular/core';
import { ApiqueryService } from './apiquery.service';
import { Router } from '@angular/router';

export interface UserInterface {
    loginId: string;
    password: string;
    role: string;
    remote?: any;
}

export interface SessionInterface {
    isLoggedIn: boolean;
    user: UserInterface;
}

@Injectable()
export class UserService {
    workerStarted: any;
    public $sessionChange: EventEmitter<SessionInterface> = new EventEmitter();

    public session: SessionInterface = this.getLogoutModel();

    constructor(private apiQuery: ApiqueryService, private router: Router) {
        this.$sessionChange.subscribe(() => { this.onSessionChange(); }, () => { });
    }

    startWorker() {
        if (this.workerStarted) { return; }
        const toSession = JSON.parse(localStorage.getItem('userService'));
        if (toSession) { this.session = toSession; }

        this.$sessionChange.emit(this.session);
        this.workerStarted = true;
    }

    login(user: UserInterface): boolean {

        // Login exclusivo administrador
        if (user.loginId == "admin") {
            if (user.password == "admin") {
                this.session.user = user;
                this.session.user.role = "ROLE_ADMIN";
                this.session.isLoggedIn = true;
                this.$sessionChange.emit(this.session);
                return true;
            } else {
                return false;
            }
        }

        // TODO: Connect to real API
        

        this.session.user = user;
        this.onSessionChange();

        this.apiQuery.getUser().subscribe((response) => {
            this.session.user.remote = response['User/Get'];
            this.session.user.role = this.session.user.remote.Role;
            if (this.session.user.role=="ROLE_VETERINARIO") {
              this.router.navigateByUrl('/veterinarios');
            }
            if (this.session.user.role=="ROLE_CLIENTE") {
              this.router.navigateByUrl('/cliente');
            }
            this.session.isLoggedIn = true;
            this.$sessionChange.emit(this.session);
        }, () => {
            this.logout();
        });

        return true;
    }

    logout() {
        this.session = this.getLogoutModel();
        this.session.isLoggedIn = false;
        this.$sessionChange.emit(this.session);
    }

    getLogoutModel(): SessionInterface {
        return { isLoggedIn: false, user: null };
    }


    getSession(): SessionInterface {
        return this.session;
    }

    setNewPassword(newpass: string) {
        this.session.user.password = newpass;
        this.onSessionChange();
    }

    setNewRemote(newRemote) {
        this.session.user.remote = newRemote;
        this.onSessionChange();
    }

    private onSessionChange() {
        localStorage.setItem('userService', JSON.stringify(this.session));
    }

}
