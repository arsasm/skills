import { Injectable, EventEmitter } from '@angular/core';

@Injectable()
export class KeyEventsService {
  public keyPressed$: EventEmitter<KeyboardEvent> = new EventEmitter();
  
  addEvent(type: string, event: KeyboardEvent) {
    if (type=="keyPressed") {
      this.keyPressed$.emit(event);
    }
  }

}
